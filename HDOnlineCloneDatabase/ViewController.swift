//
//  ViewController.swift
//  HDOnlineCloneDatabase
//
//  Created by Nguyen Tien Dung on 9/9/15.
//  Copyright (c) 2015 Nguyen Tien Dung. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textLog: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func logText(text : String){
        
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            var text_ = self.textLog.text
            text_ = text_ + "\n\(text)..."
            self.textLog.text = text_
            let p : CGPoint = self.textLog.contentOffset
            self.textLog.contentOffset = p
            self.textLog.scrollRangeToVisible(NSMakeRange(count(self.textLog.text),0))
        });
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.logText("Start Clone");
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), { () -> Void in
            if let DataHomePage : NSData = NSData(contentsOfURL: NSURL(string: "http://hdonline.vn/")!){
                // Lấy chuyên mục
                if let documentRoot : TFHpple = TFHpple(HTMLData: DataHomePage){
                    if let ListRoot : NSArray = documentRoot.searchWithXPathQuery("//div[@class='gnavsub']"){
                        for r1 in ListRoot{
                            if let elmentRoot : TFHppleElement = r1 as? TFHppleElement{
                                if let TagULRoot : TFHppleElement = elmentRoot.childrenWithTagName("ul").last as? TFHppleElement{
                                    if let listLiRoot : NSArray = TagULRoot.childrenWithTagName("li"){
                                        for i in 0..<listLiRoot.count{
                                            
                                            if let elementLi : TFHppleElement = listLiRoot[i] as? TFHppleElement{
                                                if let elementa : TFHppleElement = elementLi.childrenWithTagName("a").last as? TFHppleElement{
                                                    if let href : String = elementa.attributes["href"] as? String{
                                                        
                                                        self.logText(elementa.text())
                                                        if(href.hasSuffix(".html")){
                                                            self.getPageAndFilm(href)
                                                        }
                                                        
                                                    }
                                                    
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        self.logText("Done")
                    }
                }
                
            }

        })
        
    }
    func getPageAndFilm(urlPage : String){
        var fullURL : String = urlPage;
        if(!fullURL.hasPrefix("http")){
            fullURL = "http://hdonline.vn" + urlPage
        }
        self.logText(fullURL)
        if let DataHomePage : NSData = NSData(contentsOfURL: NSURL(string: fullURL)!){
            if let documentRoot : TFHpple = TFHpple(HTMLData: DataHomePage){
                if let ListFilm : NSArray = documentRoot.searchWithXPathQuery("//a[@class='jt bxitem-link']"){
                    for i in 0..<ListFilm.count{
                        if let film : TFHppleElement = ListFilm[i] as? TFHppleElement{
                            for span in film.children{
                                if((span as? TFHppleElement)?.children.count != 0){
                                    if let img : TFHppleElement = (span as! TFHppleElement).childrenWithTagName("img").last as? TFHppleElement{
                                        if let imgLink : String = img.attributes["src"] as? String{
                                            self.getFilmAndSubmit(film.attributes["href"] as! String,poster: imgLink)
                                        }
                                    }
                                }
                            }
                           
                            
                        }
                    }
                }
                if let nextPage : NSArray = documentRoot.searchWithXPathQuery("//a[@rel='nofollow']"){
                    for nextLink in nextPage{
                        
                        if("Trang Sau" == (nextLink as! TFHppleElement).text()){
                            self.getPageAndFilm((nextLink as! TFHppleElement).attributes["href"] as! String)
                        }
                    }
                }
            }
        }
    }
    
   
    
    func getFilmAndSubmit(url : String,poster : String){
        var fullURL : String = url;
        if(!fullURL.hasPrefix("http")){
            fullURL = "http://hdonline.vn" + url
        }
        logText(fullURL)
        if let DataHomePage : NSData = NSData(contentsOfURL: NSURL(string: fullURL)!){
            
            if let documentRoot : TFHpple = TFHpple(HTMLData: DataHomePage){
                var filmUUID : Int = 0
                var filmName : String = ""
                var filmEngName : String = ""
                var filmType : Array<String> = Array<String>()
                var filmcountry : Array<String> = Array<String>()
                var filmTime : String = ""
                var filmYear : String = ""
                var filmDriector : Array<String> = Array<String>()
                var filmIMDBScore : String = ""
                var filmDescription : String = ""
                var filmTrailer : String = ""
                var filmSugestionsFilm : Array<NSDictionary> = Array<NSDictionary>()
                var filmScreenshot : Array<String> = Array<String>()
               
                filmUUID = fullURL.componentsSeparatedByString("-").last!.componentsSeparatedByString(".").first!.toInt()!
                if let ELementInfo : TFHppleElement = documentRoot.searchWithXPathQuery("//ul[@class='filminfo-fields']").last as? TFHppleElement{
                    let arrChi = ELementInfo.childrenWithTagName("li")
                    for i in 0..<arrChi.count{
                        if let li : TFHppleElement = arrChi[i] as? TFHppleElement{
                            if("Tên Phim: " == li.text()){
                                if let h1 : TFHppleElement = li.childrenWithTagName("h1").last as? TFHppleElement{
                                    if let strong : TFHppleElement = h1.childrenWithTagName("strong").last as? TFHppleElement{
                                        filmName = strong.text()
                                    }
                                }
                            }
                            else if("Tên Tiếng Anh: " == li.text()){
                                if let h2 : TFHppleElement = li.childrenWithTagName("h2").last as? TFHppleElement{
                                    if let strong : TFHppleElement = h2.childrenWithTagName("strong").last as? TFHppleElement{
                                        filmEngName = strong.text()
                                    }
                                }
                            }
                            else if("Năm sản xuất: " == li.text()){
                                if let a : TFHppleElement = li.childrenWithTagName("a").last as? TFHppleElement{
                                   filmYear = a.text()
                                }
                            }
                            else if("Thể loại: " == li.text()){
                                if let strong : TFHppleElement = li.childrenWithTagName("strong").last as? TFHppleElement{
                                    for aLink in strong.childrenWithTagName("a"){
                                        filmType.append((aLink as! TFHppleElement).text())
                                    }
                                }
                            }
                            else if("Quốc gia: " == li.text()){
                                if let strong : TFHppleElement = li.childrenWithTagName("strong").last as? TFHppleElement{
                                    for aLink in strong.childrenWithTagName("a"){
                                        filmcountry.append((aLink as! TFHppleElement).text())
                                    }
                                }
                            }
                            else if(li.text().hasPrefix("Thời lượng:")){
                                filmTime = li.text()
                            }
                            else if("Đạo diễn: " == li.text()){
                                for aLink in li.childrenWithTagName("a"){
                                    filmDriector.append((aLink as! TFHppleElement).text())
                                }
                            }
                            else if(li.childrenWithTagName("img").count != 0){
                                if let img : TFHppleElement = li.childrenWithTagName("img").last as? TFHppleElement{
                                    filmIMDBScore = li.text().stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
                                }
                            }
                            //Thời lượng:
                        }
                    }
                    
                }
                if let ELementDes : TFHppleElement = documentRoot.searchWithXPathQuery("//div[@class='tn-contentmt maxheightline-6']").last as? TFHppleElement{
                    if let p : TFHppleElement = ELementDes.childrenWithTagName("p").last as? TFHppleElement{
                        
                        filmDescription = p.raw.stringByStrippingHTML.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
                    }
                }
                if let ELementIframe : TFHppleElement = documentRoot.searchWithXPathQuery("//iframe").last as? TFHppleElement{
                    if let youtubeLink : String = ELementIframe.attributes["src"] as? String{
                        if let youtubeID : String = youtubeLink.componentsSeparatedByString("watch?v=").last{
                            filmTrailer = youtubeID
                        }
                    }
                }
                if let ListfilmSugestion : NSArray = documentRoot.searchWithXPathQuery("//a[@class='bxitem-link']"){
                    for i in 0..<ListfilmSugestion.count{
                        var uuid : Int = 0
                        var film_sug_Name : String = ""
                        var film_sug_Poster : String = ""
                        if let film : TFHppleElement = ListfilmSugestion[i] as? TFHppleElement{
                            if let Link : String = film.attributes["href"] as? String{
                                uuid =  Link.componentsSeparatedByString("-").last!.componentsSeparatedByString(".").first!.toInt()!
                            }
                            if let p : TFHppleElement = film.childrenWithTagName("p").last as? TFHppleElement{
                                if let fName : String = p.attributes["title"] as? String{
                                    film_sug_Name = fName
                                }
                            }
                            if let span : TFHppleElement = film.childrenWithTagName("span").last as? TFHppleElement{
                                if let img : TFHppleElement = span.childrenWithTagName("img").last as? TFHppleElement{
                                    if let poster : String = img.attributes["src"] as? String{
                                        film_sug_Poster = poster
                                    }
                                }
                            }
                        }
                        filmSugestionsFilm.append(["uuid":uuid,"name":film_sug_Name,"filmPoster":film_sug_Poster])
                    }
                }
                if let urlScreenShot : TFHppleElement = documentRoot.peekAtSearchWithXPathQuery("//ul[@class='tn-uldef image-screenshot']"){
                    if let listscreen :  NSArray = urlScreenShot.childrenWithTagName("li"){
                        for i in 0..<listscreen.count{
                            if let li : TFHppleElement = listscreen[i] as? TFHppleElement{
                                if let img : TFHppleElement = li.childrenWithTagName("img").last as? TFHppleElement{
                                    if let poster : String = img.attributes["src"] as? String{
                                        filmScreenshot.append(poster)
                                    }
                                }
                            }
                        }
                    }
                }
                
                
                var filmMetaData : NSDictionary = ["uuid":filmUUID,
                    "url":fullURL,
                    "name":filmName,
                    "description":filmDescription,
                    "type":filmType,
                    "time":filmTime,
                    "year":filmYear,
                    "imdb":filmIMDBScore,
                    "bannerImage":"",
                    "poster":poster,
                    "engName":filmEngName,
                    "director":filmDriector,
                    "tailer":filmTrailer,
                    "sugestion":filmSugestionsFilm,
                    "screenshot":filmScreenshot
                ]
                var er : NSError
                
                PFCloud.callFunction("onSaveFilm", withParameters: filmMetaData as [NSObject : AnyObject])
                logText(filmName)
            }
        }
    }

}

